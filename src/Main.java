import service.GenerationService;

import java.util.Scanner;

/**
 * JARVIS PRIME-NUMBER GENERATING APP.
 * SIMPLY RUN THE APPLICATION FROM THE COMMAND LINE AND FOLLOW THE PROMPTS
 */



public class Main {

    public static void main(String[] args)
    {
        displayMenu();
    }


    public static void displayMenu()
    {
        Scanner in = new Scanner(System.in);
        System.out.println("\n\n\n\n\n\n*****WELCOME TO JARVIS PRIME NUMBER GENERATOR*****\n\n");
        System.out.println("Start by defining your prime number range.");



        int startNo = -1;
        int endNo = -1;
        int selection = -1;


        //Show prompts and accept input from user
        try {
            System.out.println("* Lowest number:");
            startNo = in.nextInt();

            System.out.println("* Highest number: ");
            endNo = in.nextInt();

            System.out.println("* Jarvis has 3 methods for generating these prime numbers. Please make a selection between 1 and 3:");
            selection = in.nextInt();
            while (selection < 1 || selection > 3)
            {
                System.err.println("Ops! You need to make a selection between 1 and 3 only");
                selection = in.nextInt();
            }


            if(startNo >= endNo){
                System.err.println("Ops! The first number must not be greater than or equals to the last number!");
                displayMenu();
            }
        } catch (Exception e) {
            System.err.println("Ops! Input error. Please try again.");
            displayMenu();
        }



        //Generate the prime numbers per the selection
        GenerationService gService = new GenerationService();
        if(selection == 1){
            gService.generatePrimeNo1(startNo, endNo);
        }
        else if(selection == 2){
            gService.generatePrimeNo2(startNo, endNo);
        }
        else if(selection == 3){
            gService.generatePrimeNo3(startNo, endNo);
        }

        displayMenu();

    }
}
