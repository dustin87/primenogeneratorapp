package service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GenerationService
{
    public void generatePrimeNo1(int low, int high)
    {
        //validation
        if(low >= high){
            System.out.println("The first number must be lower than the second number");
            return;
        }

        if(high == 1){
            System.out.println("No Prime numbers");
            return;
        }


        //generate prime numbers
        String primeNumbers = "";
        for (int i = low; i <= high; i++ )
        {
            if(i == 0 || i == 1) {
                continue;
            }


            //second check
            boolean isPrime2 = true;
            for(int num = 2; num < i; num++) {
                if(i % num == 0)
                {
                    isPrime2 = false;
                }
            }

            if(isPrime2)
            {
                primeNumbers += i + ", ";
            }
        }


        if(primeNumbers.equals("")){
            System.out.println("NO PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high);
        }else{
            System.out.println("PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high +":\n"+ primeNumbers);
        }
    }

    public void generatePrimeNo2(int low, int high)
    {

        //validation
        if(low >= high){
            System.out.println("The first number must be lower than the second number");
            return;
        }

        if(high == 1){
            System.out.println("No Prime numbers");
            return;
        }


        //generate prime numbers
        String primeNumbers = "";
        for (int i = low; i <= high; i++ )
        {
            if(i == 0 || i == 1) {
                continue;
            }


            //second check
            boolean isPrime2 = true;
            for(int num = 2; num < i; num++)
            {
                double d = (double)i / (double)num;
                if((d - (int)d) == 0){
                    isPrime2 = false;
                }
            }

            if(isPrime2 == true)
            {
                primeNumbers = primeNumbers + i + ", ";
            }
        }


        if(primeNumbers.equals("")){
            System.out.println("NO PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high);
        }else{
            System.out.println("PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high +":\n"+ primeNumbers);
        }

    }

    public void generatePrimeNo3(int low, int high)
    {

        //validation
        if(low >= high){
            System.out.println("The first number must be lower than the second number");
            return;
        }

        if(high == 1){
            System.out.println("No Prime numbers");
            return;
        }


        //generate prime numbers
        StringBuilder st = new StringBuilder();
        for (int i = low; i <= high; i++ )
        {
            if(i == 0 || i == 1) {
                continue;
            }


            //second check
            boolean isPrime2 = true;
            for(int num = 2; num < i; num++)
            {
                if(i % num == 0)
                {
                    isPrime2 = false;
                }
            }

            if(isPrime2)
            {
                st.append(i);
                st.append(", ");
            }
        }


        if(st == null){
            System.out.println("NO PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high);
        }else{
            System.out.println("PRIME NUMBERS FOUND WITHIN THE RANGE OF "+ low + " and "+ high +":\n"+ st);
        }

    }
}
